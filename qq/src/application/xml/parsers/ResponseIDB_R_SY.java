package application.xml.parsers;

import java.io.BufferedReader;
import java.io.File;

import java.io.FileReader;
import java.io.IOException;


import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ResponseIDB_R_SY {
	private Map<String, String> initialRegNumber;
	private String service;
	private Map<String, String> regNumber;
	private String regHistory;
	private String documentAuthorOwsName;
	private Map<String, String> documentAuthorOwsAddress;
	private Map<String, String> documentAuthorOwsEcontact;
	private String documentAuthorOwsOpwsName;
	private Map<String, String> documentAuthorOwsOpwsOfficial;
	private String documentAuthorOwsOpwsSignDate;
	
	
	public Map<String, String> getInitialRegNumber() {
		return initialRegNumber;
	}

	public String getService() {
		return service;
	}

	public Map<String, String> getRegNumber() {
		return regNumber;
	}

	public String getRegHistory() {
		return regHistory;
	}

	public String getDocumentAuthorOwsName() {
		return documentAuthorOwsName;
	}

	public Map<String, String> getDocumentAuthorOwsAddress() {
		return documentAuthorOwsAddress;
	}

	public Map<String, String> getDocumentAuthorOwsEcontact() {
		return documentAuthorOwsEcontact;
	}

	public String getDocumentAuthorOwsOpwsName() {
		return documentAuthorOwsOpwsName;
	}

	public Map<String, String> getDocumentAuthorOwsOpwsOfficial() {
		return documentAuthorOwsOpwsOfficial;
	}

	public String getDocumentAuthorOwsOpwsSignDate() {
		return documentAuthorOwsOpwsSignDate;
	}

	public ResponseIDB_R_SY(String xml) throws ParserConfigurationException, SAXException, IOException {
		//File file = new File("D:\\responseIDB_F_SY.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		InputSource is = new InputSource(new StringReader(xml));
		Document doc = dBuilder.parse(is);
		//Document doc = dBuilder.parse(file);
		
		Element e = doc.getDocumentElement();
		// Header Document Expansion
		NodeList nList = e.getChildNodes();
		for (int i=0; i<nList.getLength(); i++) {
			Node nd = nList.item(i);
			if (nd instanceof Element) {
				Element el = (Element) nd;
				//System.out.println(el.getNodeName());
				if (el.getNodeName().equals("Header")) 
					parseHeader(el);
				else if (el.getNodeName().equals("Document"))
					parseDocument(el);
				else if (el.getNodeName().equals("Expansion"))
					parseExpansion(el);
			}
		}
	}
	
	private void parseHeader(Element e) {
		NodeList nList = e.getChildNodes();
		for (int i=0; i<nList.getLength(); i++) {
			Node nd = nList.item(i);
			if (nd instanceof Element) {
				Element el = (Element)nd;
				//System.out.println(el.getNodeName());
				if (el.getNodeName().equals("InitialRegNumber"))
					parseHeader_InitialRegNumber(el);
				else if (el.getNodeName().equals("Service"))
					parseHeader_Service(el);
			}
		}
	}
	
	private void parseHeader_InitialRegNumber(Element e) {
		initialRegNumber = new HashMap<String, String>();
		initialRegNumber.put("regtime", e.getAttribute("regtime"));
		initialRegNumber.put("content", e.getTextContent());
		System.out.println("--InitilaRegNumber: " + initialRegNumber);
	}
	private void parseHeader_Service(Element e) {
		service = e.getTextContent();
		System.out.println("--Service: " + service);
	}
	
	private void parseDocument(Element e) {
		// RegNumber RegHistory Author
		NodeList nList = e.getChildNodes();
		for (int i=0; i<nList.getLength(); i++) {
			Node nd = nList.item(i);
			if (nd instanceof Element) {
				Element el = (Element) nd;
				System.out.println(el.getNodeName());
				if (el.getNodeName().equals("RegNumber"))
					parseDocument_RegNumber(el);
				else if (el.getNodeName().equals("RegHistory"))
					parseDocument_RegHistory(el);
				else if (el.getNodeName().equals("Author"))
					parseDocmuent_Author(el);
			}
		}
	}
	private void parseDocument_RegNumber(Element e) {
		regNumber = new HashMap<String, String>();
		regNumber.put("regtime", e.getAttribute("regtime"));
		regNumber.put("content", e.getTextContent());
		System.out.println("--RegNumber: " + regNumber);
	}
	private void parseDocument_RegHistory(Element e) {
		regHistory = e.getTextContent();
		System.out.println("--RegHistory: " + regHistory);
	}
	private void parseDocmuent_Author(Element e) {
		NodeList nList = e.getChildNodes();
		for (int i=0; i<nList.getLength(); i++) {
			Node nd = nList.item(i);
			if (nd instanceof Element) {
				Element el = (Element) nd;
				System.out.println(el.getNodeName());
				if (el.getNodeName().equals("OrganizationWithSign"))
					parseDocument_Author_OWS(el);
			}
		}
	}
	private void parseDocument_Author_OWS(Element e){
		NodeList nList = e.getChildNodes();
		for (int i=0; i<nList.getLength(); i++) {
			Node nd = nList.item(i);
			if (nd instanceof Element) {
				Element el = (Element) nd;
				//System.out.println(el.getNodeName());
				if (el.getNodeName().equals("Name"))
					parseDocument_Author_Ows_Name(el);
				else if (el.getNodeName().equals("Address"))
					parseDocument_Author_Ows_Address(el);
				else if (el.getNodeName().equals("Econtact"))
					parseDocument_Author_Ows_Econtact(el);
				else if (el.getNodeName().equals("OfficialPersonWithSign"))
					parseDocument_Author_Ows_Opws(el);
			}
		}
	}
	private void parseDocument_Author_Ows_Name(Element e) {
		documentAuthorOwsName = e.getTextContent();
		System.out.println("---documentAuthorOwsName: " + documentAuthorOwsName);
	}
	private void parseDocument_Author_Ows_Address(Element e) {
		NodeList nList = e.getChildNodes();
		documentAuthorOwsAddress = new HashMap<String, String>();
		for (int i=0; i<nList.getLength(); i++) {
			Node nd = nList.item(i);
			if (nd instanceof Element) {
				documentAuthorOwsAddress.put(nd.getNodeName(), nd.getTextContent());
				//System.out.println(nd.getNodeName());
			}
		}
		System.out.println("Address: " + documentAuthorOwsAddress);
	}
	private void parseDocument_Author_Ows_Econtact(Element e) {
		documentAuthorOwsEcontact = new HashMap<String, String>();
		documentAuthorOwsEcontact.put("type", e.getAttribute("type"));
		documentAuthorOwsEcontact.put("context", e.getTextContent());
		System.out.println("Econtact: " + documentAuthorOwsEcontact);
	}
	private void parseDocument_Author_Ows_Opws(Element e) {
		NodeList nList = e.getChildNodes();
		for (int i=0; i<nList.getLength(); i++) {
			Node nd = nList.item(i);
			if (nd instanceof Element) {
				Element el = (Element)nd;
				if (el.getNodeName().equals("Name"))
					parseDocument_Author_Ows_Opws_Name(el);
				else if (el.getNodeName().equals("Official"))
					parseDocument_Author_Ows_Opws_Official(el);
				else if (el.getNodeName().equals("SignDate"))
					parseDocument_Author_Ows_Opws_SignDate(el);
			}
		}
	}
	private void parseDocument_Author_Ows_Opws_Name(Element e){
		documentAuthorOwsOpwsName = e.getTextContent();
		System.out.println("Document_Author_Ows_Opws_Name: " + documentAuthorOwsOpwsName);
	}
	private void parseDocument_Author_Ows_Opws_Official(Element e) {
		documentAuthorOwsOpwsOfficial = new HashMap<String, String>();
		documentAuthorOwsOpwsOfficial.put("department", e.getAttribute("department"));
		documentAuthorOwsOpwsOfficial.put("post", e.getAttribute("post"));
		documentAuthorOwsOpwsOfficial.put("content", e.getTextContent());
		System.out.println("Dcoument_Author_Ows_Opws_Official: " + documentAuthorOwsOpwsOfficial);
	}
	private void parseDocument_Author_Ows_Opws_SignDate(Element e) {
		documentAuthorOwsOpwsSignDate = e.getTextContent();
		System.out.println("Document_Author_Ows_Opws_SignDate: " + documentAuthorOwsOpwsSignDate);
	}
	
	private void parseExpansion(Element e) {
		
	}
	
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		//ResponseIDB_R_SY resp = new ResponseIDB_R_SY();
		BufferedReader r = new BufferedReader(new FileReader(new File("D:\\responseIDB_F_SY.xml")));
		String s;
		StringBuilder sb = new StringBuilder();
		while ( (s=r.readLine()) != null) {
			//System.out.println(s);
			sb.append(s);
		}
		r.close();
		
		// Using:
		ResponseIDB_R_SY resp = new ResponseIDB_R_SY(sb.toString());
		System.err.println(resp.getDocumentAuthorOwsOpwsSignDate());
		System.err.println(resp.getDocumentAuthorOwsOpwsOfficial());
	}
}
