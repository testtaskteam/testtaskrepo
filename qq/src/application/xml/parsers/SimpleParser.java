package application.xml.parsers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class SimpleParser {
	public static String parse(String xml, String type) throws SAXException, IOException, ParserConfigurationException {
		if ("РО".equals(type.toUpperCase()))
			return parseRo(xml);
		else if ("СУ".equals(type.toUpperCase()))
			return parseSy(xml);
		else if ("АД".equals(type.toUpperCase()))
			return parseAd(xml);
		else return "Unknown respose type.";
	}
	
	private static Map<String, String> parseGeneral(String xml) throws ParserConfigurationException, SAXException, IOException {
		Map<String, String> m = new HashMap<String, String>();
		xml = "<r>" + xml + "</r>";
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		InputSource is = new InputSource(new StringReader(xml));
		Document doc = dBuilder.parse(is);
		
		Element e = doc.getDocumentElement();
		NodeList nList = e.getChildNodes();
		for(int i=0; i<nList.getLength(); i++) {
			Node nd = nList.item(i);
			if (nd instanceof Element) {
				Element el = (Element) nd;
				//System.out.println(el.getNodeName() + " " + el.getTextContent());
				m.put(el.getNodeName(), el.getTextContent());
			}
		}
		return m;
	}
	private static String parseRo(String xml) throws SAXException, IOException, ParserConfigurationException {
		Map<String, String> m = parseGeneral(xml);
		//System.out.println(m);
		StringBuilder sb = new StringBuilder();
		sb.append("Орган ответивший: ");
		sb.append(m.get("organ"));
		sb.append("\n");
		sb.append("Фамилия: ");
		sb.append(m.get("secname"));
		sb.append("\n");
		sb.append("Имя: ");
		sb.append(m.get("firstname"));
		sb.append("\n");
		sb.append("Отчество: ");
		sb.append(m.get("fathersname"));
		sb.append("\n");
		sb.append("Дата рождения: ");
		sb.append(m.get("dateofbirth"));
		sb.append("\n");
		sb.append("Место рождения: ");
		sb.append(m.get("birthplace"));
		sb.append("\n");
		sb.append("Статус розыска: ");
		if ("YES".equals(m.get("insearch").toUpperCase())) {
			sb.append("ДА");
		} else
			sb.append("НЕТ");
		sb.append("\n");
		sb.append("Вид розыска: ");
		sb.append(m.get("searchkind"));
		sb.append("\n");
		sb.append("Инициатор розыска: ");
		sb.append(m.get("searchinitiator"));
		sb.append("\n");
		sb.append("Дата проверки: ");
		sb.append(m.get("checkdate"));
		sb.append("\n");
		return sb.toString();
		
	}
	private static String parseSy(String xml) throws ParserConfigurationException, SAXException, IOException {
		Map<String, String> m = parseGeneral(xml);
		//System.out.println(m);
		StringBuilder sb = new StringBuilder();
		sb.append("Орган ответивший: ");
		sb.append(m.get("organ"));
		sb.append("\n");
		sb.append("Фамилия: ");
		sb.append(m.get("secname"));
		sb.append("\n");
		sb.append("Имя: ");
		sb.append(m.get("firstname"));
		sb.append("\n");
		sb.append("Отчество: ");
		sb.append(m.get("fathersname"));
		sb.append("\n");
		sb.append("Дата рождения: ");
		sb.append(m.get("dateofbirth"));
		sb.append("\n");
		sb.append("Место рождения: ");
		sb.append(m.get("birthplace"));
		sb.append("\n");
		sb.append("Отметка о судимости: ");
		if ("YES".equals(m.get("conviction").toUpperCase())) {
			sb.append("ДА");
		} else
			sb.append("НЕТ");
		sb.append("\n");
		sb.append("Дата суда: ");
		sb.append(m.get("convictionDate"));
		sb.append("\n");
		sb.append("Статья осуждения: ");
		sb.append(m.get("article"));
		sb.append("\n");
		sb.append("Дата проверки: ");
		sb.append(m.get("checkdate"));
		sb.append("\n");
		return sb.toString();
	}
	private static String parseAd(String xml) throws ParserConfigurationException, SAXException, IOException {
		Map<String, String> m = parseGeneral(xml);
		//System.out.println(m);
		StringBuilder sb = new StringBuilder();
		sb.append("Орган ответивший: ");
		sb.append(m.get("organ"));
		sb.append("\n");
		sb.append("Фамилия: ");
		sb.append(m.get("secname"));
		sb.append("\n");
		sb.append("Имя: ");
		sb.append(m.get("firstname"));
		sb.append("\n");
		sb.append("Отчество: ");
		sb.append(m.get("fathersname"));
		sb.append("\n");
		sb.append("Дата рождения: ");
		sb.append(m.get("dateofbirth"));
		sb.append("\n");
		sb.append("Место рождения: ");
		sb.append(m.get("birthplace"));
		sb.append("\n");
		sb.append("Отметка об адмнарушении: ");
		if ("YES".equals(m.get("admin").toUpperCase())) {
			sb.append("ДА");
		} else
			sb.append("НЕТ");
		sb.append("\n");
		sb.append("Дата суда: ");
		sb.append(m.get("adminDate"));
		sb.append("\n");
		sb.append("Статья осуждения: ");
		sb.append(m.get("article"));
		sb.append("\n");
		sb.append("Дата проверки: ");
		sb.append(m.get("checkdate"));
		sb.append("\n");
		return sb.toString();
	}
	
	private static String getXmlStringAD() throws IOException {
		BufferedReader r = new BufferedReader(new FileReader(new File("D:\\АД.xml")));
		String s;
		StringBuilder sb = new StringBuilder();
		while ( (s=r.readLine()) != null) {
			//System.out.println(s);
			sb.append(s);
		}
		r.close();
		return sb.toString();
	}
	private static String getXmlStringRO() throws IOException {
		BufferedReader r = new BufferedReader(new FileReader(new File("D:\\РО.xml")));
		String s;
		StringBuilder sb = new StringBuilder();
		while ( (s=r.readLine()) != null) {
			//System.out.println(s);
			sb.append(s);
		}
		r.close();
		return sb.toString();
	}
	private static String getXmlStringCY() throws IOException {
		BufferedReader r = new BufferedReader(new FileReader(new File("D:\\СУ.xml")));
		String s;
		StringBuilder sb = new StringBuilder();
		while ( (s=r.readLine()) != null) {
			//System.out.println(s);
			sb.append(s);
		}
		r.close();
		return sb.toString();
	}
	
	public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException {
		String xmlStringRO = SimpleParser.getXmlStringRO();
		String xmlStringAD = SimpleParser.getXmlStringAD();
		String xmlStringCY = SimpleParser.getXmlStringCY();
		System.out.println("---------Розыск-------------");
		System.out.println(SimpleParser.parse(xmlStringRO, "РО"));
		System.out.println("---------Судимость-----------");
		System.out.println(SimpleParser.parse(xmlStringCY, "СУ"));
		System.out.println("---------Адм-------------");
		System.out.println(SimpleParser.parse(xmlStringAD, "АД"));
	}
}
