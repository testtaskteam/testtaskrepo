package application;

import java.io.File;
import java.io.IOException;







import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ParseXml
{
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		File file = new File("D:\\response.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(file);
		//doc.getDocumentElement().normalize();
		System.out.println(doc.getDocumentElement().getNodeName());
		
		/*
		NodeList nList = doc.getElementsByTagName("Header");
		for (int temp=0; temp<nList.getLength(); temp++) {
			Node n = nList.item(temp);
			
			System.out.println(n.getNodeName());
		}
		
		nList = doc.getElementsByTagName("Document");
		for (int temp=0; temp<nList.getLength(); temp++) {
			Node n = nList.item(temp);
			
			System.out.println(n.getNodeName());
		}
		*/
		
		Element e = doc.getDocumentElement();
		NodeList n = e.getChildNodes();
		for (int i=0; i< n.getLength(); i++) {
			Node nn = n.item(i);
			if (nn instanceof Element) {
				Element child = (Element) nn;
				NodeList nl = child.getChildNodes();
				for (int j=0; j<nl.getLength(); j++) {
					Node nnl = nl.item(j);
					if (nnl instanceof Element) {
						Element el = (Element) nnl;
						System.out.println(nnl);
						if (el.getNodeName().equals("InitialRegNumber"))
							parseInitialRegNumber(el);
						else if (el.getNodeName().equals("Service")) 
							parseService(el);
						else if (el.getNodeName().equals("RegNumber"))
							parseRegnumber(el);
						else if (el.getNodeName().equals("RegHistory"))
							parseRegHistory(el);
						else if (el.getNodeName().equals("Author"))
							parseAuthor(el);
					}
				}
				System.out.println(child);
			}
		}
		System.out.println(n.getLength());
	}
	
	private static String parseInitialRegNumber(Element element) {
		String regTime = element.getAttribute("regtime");
		String content = element.getTextContent();
		System.out.println(regTime + " " + content);
		String result = "";
		return result;
	}
	
	private static String parseService(Element element) {
		String service = element.getTextContent();
		System.out.println("---Service:" + service);
		return service;
	}
	
	private static String parseRegnumber(Element element) {
		String regTime = element.getAttribute("regtime");
		String regNumber = element.getTextContent();
		System.out.println(regTime + " " + regNumber);
		return regTime + " " + regNumber;
	}
	
	private static String parseRegHistory(Element element) {
		String regHistory = element.getTextContent();
		System.out.println("---RegHistory: " + regHistory);
		return regHistory;
	}
	private static String parseAuthor(Element element) {
		NodeList n = element.getChildNodes();
		for (int i=0; i<n.getLength(); i++) {
			Node nn = n.item(i);
			if (nn instanceof Element) {
				Element el = (Element)nn;
				//System.out.println("   " + el);
				//System.out.println("   "+ nn);
				NodeList nl = nn.getChildNodes();
				for (int j=0; j<nl.getLength(); j++) {
					Node nnn = nl.item(j);
					if (nnn instanceof Element) {
						Element nnl = (Element)nnn;
						System.out.println("   " + nnl);
						if (nnl.getNodeName().equals("Name"))
							parseAuthorName(nnl);
						else if (nnl.getNodeName().equals("Address"))
							parseAuthorAddress(nnl);
						else if (nnl.getNodeName().equals("Econtact"))
							parseAuthorEcontact(nnl);
						else if (nnl.getNodeName().equals("OfficialPersonWithSign"))
							parseAuthorOfficialPresonWithSign(nnl);
					}
				}
			}
		}
		
		String result = "";
		return result;
	}
	
	private static String parseAuthorName(Element element) {
		String result = element.getTextContent();
		System.out.println("Name: " + result);
		return result;
	}
	
	private static String parseAuthorAddress(Element element) {
		NodeList n = element.getChildNodes();
		for (int i=0; i<n.getLength(); i++) {
			Node nn = n.item(i);
			if (nn instanceof Element) {
				Element nnl = (Element)nn;
				//System.out.println("     " + nn);
				if (nnl.getNodeName().equals("Street"))
					parseAuthorAddressStreet(nnl);
				else if (nnl.getNodeName().equals("House"))
					parseAuthorAddressHouse(nnl);
				else if (nnl.getNodeName().equals("Building"))
					parseAuthorAddressBuilding(nnl);
				else if (nnl.getNodeName().equals("Settlement"))
					parseAuthorAddressSettlement(nnl);
				else if (nnl.getNodeName().equals("Region"))
					parseAuthorAddressRegion(nnl);
				else if (nnl.getNodeName().equals("Country"))
					parseAuthorAddressCountry(nnl);
				else if (nnl.getNodeName().equals("Postcode"))
					parseAuthorAddressPostcode(nnl);
				else if (nnl.getNodeName().equals("Nontypical"))
					parseAuthorAddressNontypical(nnl);
			}
		}
		String result = "";
		return result;
	}
	
	private static String parseAuthorEcontact(Element element) {
		String type = element.getAttribute("type");
		String text = element.getTextContent();
		String result = type + " " + text;
		System.out.println("Econtact: " + result);
		return result;
	}
	
	private static String parseAuthorOfficialPresonWithSign(Element element) {
		NodeList n = element.getChildNodes();
		for (int i=0; i<n.getLength(); i++) {
			Node nn = n.item(i);
			if (nn instanceof Element) {
				Element nnl = (Element)nn;
				System.out.println("      " + nnl.getNodeName());
				if (nnl.getNodeName().equals("Name"))
					parseOPWSName(nnl);
				else if (nnl.getNodeName().equals("Official"))
					parseOPWSOfficial(nnl);
				else if (nnl.getNodeName().equals("SignDate"))
					parseOPWSSignDFate(nnl);
			}
		}
		
		String result = "";
		return result;
	}
	
	private static String parseAuthorAddressStreet(Element e) {
		String result = e.getTextContent();
		System.out.println("Street: " + result);
		return result;
	}
	private static String parseAuthorAddressHouse(Element e) {
		String result = e.getTextContent();
		System.out.println("House: " + result);
		return result;
	}
	private static String parseAuthorAddressBuilding(Element e) {
		String result = e.getTextContent();
		System.out.println("Building: " + result);
		return result;
	}
	private static String parseAuthorAddressSettlement(Element e) {
		String result = e.getTextContent();
		System.out.println("Settlement: " + result);
		return result;
	}
	private static String parseAuthorAddressRegion(Element e) {
		String result = e.getTextContent();
		System.out.println("Region: " + result);
		return result;
	}
	
	private static String parseAuthorAddressCountry(Element e) {
		String result = e.getTextContent();
		System.out.println("Country: " + result);
		return result;
	}
	
	private static String parseAuthorAddressPostcode(Element e) {
		String result = e.getTextContent();
		System.out.println("Postcode: " + result);
		return result;
	}
	
	private static String parseAuthorAddressNontypical(Element e) {
		String result = e.getTextContent();
		System.out.println("Nontypical: " + result);
		return result;
	}
	
	private static String parseOPWSName(Element e) {
		String result = e.getTextContent();
		System.out.println("Name: " + result);
		return result;
	}
	
	private static String parseOPWSOfficial(Element e) {
		String departament = e.getAttribute("department");
		String post = e.getAttribute("post");
		String text = e.getTextContent();
		String result = departament + " " + post + " " + text;
		System.out.println("Officaial: " + result);
		return result;
	}
	
	private static String parseOPWSSignDFate(Element e) {
		String result = e.getTextContent();
		System.out.println("SignDate: " + result);
		return result;
	}
	
}
