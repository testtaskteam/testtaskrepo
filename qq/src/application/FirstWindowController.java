package application;

import java.awt.Button;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.xml.sax.SAXException;

import application.xml.parsers.SimpleParser;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class FirstWindowController implements Initializable {
	
	@FXML
	private ChoiceBox<String> cb1;
	@FXML
	private ChoiceBox<String> cb2;
	@FXML
	private Text l1;
	@FXML
	private Text l2;
	@FXML
	private TextField tf1;
	@FXML
	private TextField tf2;
	@FXML
	private TextField tf3;
	@FXML
	private DatePicker dp1;
	@FXML
	private TextArea ta1;
	@FXML
	private TextArea ta2;
	
	
	private String request_To;
	private String request_Type;
	
	private String request_Person_Family;
	private String request_Person_Name;
	private String request_Person_Patronymic;
	private LocalDate request_Person_BerthDate;
		
	
	@Override
	public void initialize(URL location, ResourceBundle resources) 
	{
		final ArrayList<String> arr1 = new ArrayList<String>();
		arr1.add("АСИО");
		arr1.add("ИБД-Ф");
		arr1.add("ИБД-Р");
		request_To = arr1.get(0);
		
		cb1.setItems(FXCollections.observableArrayList(arr1));
		cb1.getSelectionModel().selectFirst();
		cb1.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() 
		{
			@Override
			public void changed(ObservableValue<? extends Number> observable,
					Number oldValue, Number newValue) {
				request_To = arr1.get((int) newValue);
			}
			
		});
		
		final ArrayList<String> arr2 = new ArrayList<String>();
		arr2.add("Розыск");
		arr2.add("Судимость");
		arr2.add("Адмпрактика");
		request_Type = arr2.get(0);
		
		cb2.setItems(FXCollections.observableArrayList(arr2));
		cb2.getSelectionModel().selectFirst();
		cb2.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() 
		{
			@Override
			public void changed(ObservableValue<? extends Number> observable,
					Number oldValue, Number newValue) {
				request_Type = arr2.get((int) newValue);
			}
			
		});
	}
	
	@FXML
	public void handleBt1(ActionEvent action) 
	{
		// 1) Читаем вбитые данные(можно с проверкой)
		// 2) Формируем запрос и выводим его в левое TextArea
		// 3) Оправлем запрас, получаем ответ
		// 4) Парсим ответ для вида типа
		/*
		   Орган ответивший: текст
		   Фамилия
		   Имя
		   Отчество
		   Дата рождения
		   Место рождения - текст
		   Статус розыска – да, нет
		   Вид розыска – текст
		   Инициатор розыска – текст
		   Дата проверки
		 */
		// У каждого ответа свой формат вывода
		
		// 1)
		readFields();
		
		// 2)
		String request = createRequest();
		ta1.setText(request);
		
		// 3) // 4) В обработчика ответа
 
		try {
			sendRORequest(request);
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
 		
	}
	
	private void readFields()
	{
		request_Person_Family     = tf1.getText();
		request_Person_Name       = tf2.getText();
		request_Person_Patronymic = tf3.getText();
		request_Person_BerthDate  = dp1.getValue();
	}
	
	private String createRequest()
	{
		// Считаем что запрос сосотоит из
		// Header
		// Document
		// AddDocuments
		// DSignature
		// Expansion
		
		// Если некоторых нет то возвращаем пустую строку
		String str2 = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
			     "<Message>\n" +
			     "\t" + createRequestHeader() + "\n" +
			     "\t" + createRequestDocument() + "\n" +
//			     "\t" + createRequestAddDocuments() + "\n" +
//			     "\t" + createRequestDSignature() + "\n" +
			     "\t" + createRequestExpansion() + "\n" +
			     "</Message>\n";

		return str2;
	}
	
	private String createRequestHeader()
	{
		String str = "<Header standart=\"\" version=\"\" time=\"\">\n" +
					 "\t<InitialRegNumber regtime=\"09.05.2011 18.48.25\">00000001</InitialRegNumber>\n" +
					 "\t<Service>weapon" + request_Person_Name + "</Service>\n" +
					 "</Header>\n";
		return str;
	}
	private String createRequestExpansion()
	{
		String rType = "";
		if (request_Type.equals("Розыск"))
		{
			rType = "РО";
		} else  
		if (request_Type.equals("Судимость"))
		{
			rType = "СУ";
		} else
		if (request_Type.equals("Адмпрактика"))
		{
			rType = "АД";
		} else {
			System.out.println("Unknown type!!!");
		}
				
		
		String str = "<Expansion>" +
				"\t" +"<RequestAnswer></RequestAnswer>" + "\n" +
				"\t" +String.format("<TypeQwery>%s</TypeQwery>", rType) + "\n" +
				"\t" +"<Writer>" + "\n" +
				"\t" +"<PrivatePerson>" + "\n" +
				"\t" +"<Name>" + "\n" +
				"\t" + "\t" +String.format("<SecName>%s</SecName>", request_Person_Family ) + "\n" +
				"\t" + "\t" +String.format("<FirstName>%s</FirstName>", request_Person_Name) + "\n" +
				"\t" + "\t" +String.format("<FathersName>%s</FathersName>", request_Person_Patronymic) + "\n" +
				"\t" +"</Name>" + "\n" +
				"\t" +String.format("<DateOfBirth>%s</DateOfBirth>", request_Person_BerthDate)+ "\n" +
				"\t" +"<Address>" + "\n" +
				"\t" + "\t" +"<Street></Street>" + "\n" +
				"\t" + "\t" +"<House></House>" + "\n" +
				"\t" + "\t" +"<Building></Building>" + "\n" +
				"\t" + "\t" +"<Flat></Flat>"+ "\n" + 
				"\t" + "\t" +"<Settlement></Settlement>"+ "\n" +
				"\t" + "\t" +"<Region></Region>"+ "\n" +
				"\t" + "\t" +"<Country></Country>"+ "\n" +
				"\t" + "\t" +"<Postcode></Postcode>"+ "\n" +
				"\t" + "\t" +"<Nontypical>" + "</Nontypical>"+ "\n" +
				"\t" +"</Address>"+ "\n" +
				"\t" +"<Econtact type=\"\"></Econtact>"+ "\n" +
				"\t" +"</PrivatePerson>"+ "\n" +
				"\t" +"</Writer>"+ "\n" +
				"\t" +"<Processing>"+ "\n" +
				"\t" +"<IsInvestigation></IsInvestigation >"+ "\n" +
				"\t" +"<InvestigationKind></InvestigationKind>"+ "\n" +
				"\t" +"<OrganizationWithSign></OrganizationWithSign>"+ "\n" +
				"\t" +"<ChekDate></ChekDate>"+ "\n" + 
				"\t" +"</Processing>"+ "\n" +
				"\t" +"</Expansion>" +  "\n" ; 
		return str;
	
	}
	private String createRequestDocument()
	{
		String regNumber = "";
		String orgName = "";
		String orgAdrStreet = "";
		String orgAdrHouse = "";
		String orgAdrBuilding = "";
		String orgAdrSettlement = "";
		String orgAdrRegion = "";
		String orgAdrCountry = "";
		String orgAdrPostCode = "";
		String orgAdrNontypical = "";
		
		String personName= "";
		
		String Str = 
				"\t" +"<Document>" + 
				"\t" +String.format("<RegNumber regtime=\"\">%s</RegNumber>", regNumber) +"\n" +
				"\t" +"<RegHistory></RegHistory>" +"\n" +
				"\t" +"<Author>" +"\n" +
				"\t" +"<OrganizationWithSign>" +"\n" +
				"\t" +String.format("<Name>%s</Name>", orgName) +"\n" +
				"\t" +"<Address>" +"\n" +
				"\t" +"\t" +String.format("<Street>%s</Street>", orgAdrStreet) +"\n" +
				"\t" +"\t" +String.format("<House>%s</House>", orgAdrHouse) +"\n" +
				"\t" +"\t" +String.format("<Building>%s</Building>", orgAdrBuilding) +"\n" +
				"\t" +"\t" +String.format("<Settlement>%s</Settlement>", orgAdrSettlement) +"\n" +
				"\t" +"\t" +String.format("<Region>%s</Region>", orgAdrRegion) +"\n" +
				"\t" +"\t" +String.format("<Country>%s</Country>", orgAdrCountry) +"\n" +
				"\t" +"\t" +String.format("<Postcode>%s</Postcode>", orgAdrPostCode) +"\n" +
				"\t" +"\t" +String.format("<Nontypical>%s</Nontypical>", orgAdrNontypical) +"\n" +
				"\t" +"</Address>" +"\n" +
				"\t" +"<Econtact type=\"\"></Econtact>" +"\n" +
				"\t" +"<OfficialPersonWithSign>" +"\n" +
				"\t" +"\t" +String.format("<Name>%s</Name>", personName) +"\n" +
				"\t" +"\t" +"<Official department=\"\" post=\"\"></Official>" +"\n" +
				"\t" +"\t" +"<SignDate></SignDate>" +"\n" +
				"\t" +"</OfficialPersonWithSign>" +"\n" +
				"\t" +"</OrganizationWithSign>" +"\n" +
				"\t" +"</Author>" +"\n" +
				"\t" +"</Document>";
		return Str;
	}
	
	
	
	private void sendRequest(String request)
	{
		String targetProtocol = "http://";
		String targetUrl = "http://thawing-hollows-5408.herokuapp.com/ibd_f/ro";

		CloseableHttpResponse response = null;
		ta2.clear();

		try {
			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpGet httpGet = new HttpGet(targetUrl);
			response = httpclient.execute(httpGet);
			//System.out.println(response1.getStatusLine());
			//ta2.setText(response.getStatusLine().toString());
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));

			StringBuilder sb = new StringBuilder();
			String line = "";
			while ((line = rd.readLine()) != null) {
				//System.out.println(line);
				//resultArea.
				sb.append(line);
				sb.append("\n");
			}
			//ta2.setText(sb.toString());
			
			// 4) Отпарсить и вывести
			// В зависимости от типа запроса выбираем вариант шаблона ответа
			
			if ( request_Type == "Розыск" )
			{
				ta2.clear();
				ta2.setText("Орган ответивший: текст\n" +
						    "Фамилия\n" +  
						    "Имя\n" + 
						    "Отчество\n" +
						    "Дата рождения\n" + 
						    "Место рождения - текст\n" + 
						    "Статус розыска – да, нет\n" +
						    "Вид розыска – текст\n" + 
						    "Инициатор розыска – текст\n" + 
						    "Дата проверки");
			}
			
			if ( request_Type == "Судимость" )
			{
				ta2.clear();
				ta2.setText("Орган ответивший: текст\n" +
						    "Фамилия\n" +  
						    "Имя\n" + 
						    "Отчество\n" +
						    "Дата рождения\n" + 
						    "Место рождения - текст\n" + 
						    "Отметка о судимости: да, нет\n" +
						    "Результат проверки1: дата осуждения\n" + 
						    "Результат проверки 1: статья\n" + 
						    "Результат проверки N: дата осуждения\n" + 
						    "Результат проверки N: статья\n" + 
						    "Дата проверки");
			}
			
			if ( request_Type == "Адмпрактика" )
			{
				ta2.clear();
				ta2.setText("Орган ответивший: текст\n" +
						    "Фамилия\n" +  
						    "Имя\n" + 
						    "Отчество\n" +
						    "Дата рождения\n" + 
						    "Место рождения - текст\n" + 
						    "Отметка об адмнарушении: да, нет\n" +
						    "Результат проверки1: дата адмнарушения\n" + 
						    "Результат проверки 1: статья\n" + 
						    "Результат проверки N: дата адмнарушения\n" + 
						    "Результат проверки N: статья\n" + 
						    "Дата проверки");
			}


		} catch (ClientProtocolException cpe) {
			ta2.setText(cpe.toString());
		} catch (IOException ioe) {
			ta2.setText(ioe.toString());
		}
		finally {
			if (response != null)
				try {
					response.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
	
	@FXML
	public void handlePost(ActionEvent action) throws SAXException, IOException, ParserConfigurationException {
		String str1 = "<organ>НКВД</organ>" +
		"<firstname>Иван</firstname>" +
		"<secname>Иванов</secname>" +
		"<fathersname>Иванович</fathersname>" +
		"<dateofbirth>10.10.1975</dateofbirth>" +
		"<birthplace>Москва</birthplace>" +
		"<insearch>YES</insearch>" +
		"<searchkind>Уголовный</searchkind>" +
		"<searchinitiator>Иванов Б.Б.</searchinitiator>" +
		"<checkdate>28.08.2014</checkdate>" +
		"<typeqwery>РО</typeqwery>";
		
		
		 String str = SimpleParser.parse(str1, "РО");
		 
		 ta2.setText(str);
	}
	
    //	@FXML
	public void handleResponce(ActionEvent action)
	{
		try {
			sendRORequest(null);
		} catch (SAXException | ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void sendRORequest(String requestBody) throws SAXException, ParserConfigurationException
	{
		String str = "<Message>" +
					"<Expansion>	<RequestAnswer></RequestAnswer>" +
					"<TypeQwery>РО</TypeQwery>" +
					"<Writer>" +
					"<PrivatePerson>" +
					"<Name>" +
						"<SecName></SecName>" +
						"<FirstName></FirstName>" +
						"<FathersName></FathersName>" +
					"</Name>" +
					"<DateOfBirth>null</DateOfBirth>" +
					"<Address>" +
						"<Street></Street>" +
						"<House></House>" +
						"<Building></Building>" +
						"<Flat></Flat>" +
						"<Settlement></Settlement>" +
						"<Region></Region>" +
						"<Country></Country>" +
						"<Postcode></Postcode>" +
						"<Nontypical></Nontypical>" +
					"</Address>" +
					"<Econtact></Econtact>" +
					"</PrivatePerson>" +
					"</Writer>" +
					"<Processing>" +
					"<IsInvestigation></IsInvestigation >" +
					"<InvestigationKind></InvestigationKind>" +
					"<OrganizationWithSign></OrganizationWithSign>" +
					"<ChekDate></ChekDate>" +
					"</Processing>" +
					"</Expansion>" +
				"</Message>";
	    
		CloseableHttpResponse response = null;
		try {
			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost("http://thawing-hollows-5408.herokuapp.com/2xml");
			httpPost.setEntity(new StringEntity(requestBody, "UTF-8"));
			httpPost.setHeader("Content-Type", "application/xml");
			
			response = httpclient.execute(httpPost);
			System.out.println(response.getStatusLine());
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			String line = "";
			StringBuilder sb = new StringBuilder();
			while ((line = rd.readLine()) != null) {
				//System.out.println(line);
				sb.append(line);
				sb.append("\n");
			}
			
			String rType = "";
			if (request_Type.equals("Розыск"))
			{
				ta2.setText(SimpleParser.parse(sb.toString(), "РО"));
			} else  
			if (request_Type.equals("Судимость"))
			{
				ta2.setText(SimpleParser.parse(sb.toString(), "СУ"));
			} else
			if (request_Type.equals("Адмпрактика"))
			{
				ta2.setText(SimpleParser.parse(sb.toString(), "АД"));
			} else {
				System.out.println("Unknown type!!!");
			}
 			

		} catch (ClientProtocolException cpe) {
			ta2.setText(cpe.toString());
		} catch (IOException ioe) {
			ta2.setText(ioe.toString());
		}
		finally {
			if (response != null)
				try {
					response.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		
//		String parsedResponseText = SimpleParser.parse(responseText, "РО");
//		ta2.setText(parsedResponseText);
	}
	
	 
	
//	@FXML
//	public void handleButton(ActionEvent action) {
//		String targetProtocol = "http://";
//		String targetUrl = urlTF.getText();
//		StringBuilder sb = new StringBuilder();
//
//		CloseableHttpResponse response1 = null;
//		resultArea.clear();
//		rescodeTF.clear();
//
//		try {
//			CloseableHttpClient httpclient = HttpClients.createDefault();
//			HttpGet httpGet = new HttpGet(targetProtocol + targetUrl);
//			response1 = httpclient.execute(httpGet);
//			//System.out.println(response1.getStatusLine());
//			rescodeTF.setText(response1.getStatusLine().toString());
//			BufferedReader rd = new BufferedReader(new InputStreamReader(
//					response1.getEntity().getContent()));
//
//			String line = "";
//			while ((line = rd.readLine()) != null) {
//				//System.out.println(line);
//				//resultArea.
//				sb.append(line);
//				sb.append("\n");
//			}
//			resultArea.setText(sb.toString());
//
//		} catch (ClientProtocolException cpe) {
//			resultArea.setText(cpe.toString());
//		} catch (IOException ioe) {
//			resultArea.setText(ioe.toString());
//		}
//		finally {
//			if (response1 != null)
//				try {
//					response1.close();
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//		}
//	}

	@FXML
	public void handleBt2(ActionEvent action) {
		ta1.clear();
	}

	@FXML
	public void handleBt3(ActionEvent action) {
		ta2.clear();
	}

}
