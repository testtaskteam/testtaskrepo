var express = require('express')
var app = express();
var xml = require('xml');
var xmldoc = require('xmldoc');

app.set('port', (process.env.PORT || 5000))
app.use(express.static(__dirname + '/public'))

app.get('/', function(request, response) {
  response.send('Hello World!')
})

app.get('/mickhan', function(request, response) {
    response.send('Hello Mickhan!')
})

app.get('/citizenAppial', function(request, response) {
    response.set('Content-Type', 'text/xml');
    var xmlString = xml({message: [{header: "Заголовок", document: "Сам документ"}]});

    //var document = new xmldoc.XmlDocument("<author><name>looooooong value</name></author>");

    if (request.body)
    {
        response.send(request.body)
    } else {
        response.send(xmlString)
    }
})

app.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port'))
})
